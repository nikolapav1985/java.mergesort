/**
*
* Class MergeSort
*
* used to perform sort by merging
*
* it takes n log(2) n steps to sort an array
*
* ----- references -----
*
* http://rosettacode.org/mw/index.php?title=Sorting_algorithms/Merge_sort&redirect=no
*
* ----- compile -----
*
* javac MergeSort.java
*
* ----- run example -----
*
* java MergeSort 5 15
*
* ----- example output -----
*
* 10 4 11 4 10 
* 4 4 10 10 11 
*
* ----- compiler version -----
*
* javac version 11.0.5
*
**/
class MergeSort{
    public static void main(String[] args){
        int len=0;
        int max=0;
        int i=0;
        int[] arr;
        int[] tmp;
        if(args.length != 2){
            System.exit(0);
        }
        len=Integer.parseInt(args[0]);
        max=Integer.parseInt(args[1]);
        arr=new int[len];
        tmp=new int[len];
        for(i=0;i<len;i++){
            arr[i]=(int)(Math.random()*max);
        }
        for(i=0;i<len;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println("");
        split(0,len-1,arr,tmp);
        for(i=0;i<len;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println("");
    }
    public static void split(int lo,int hi,int[] arr,int[] tmp){ // lo - smallest array index
        // hi - highest array index
        // arr - array to be sorted
        // tmp - temporary array
        int mid=(lo+hi)/2;
        if(lo<hi){
            split(lo,mid,arr,tmp); // get a part of the list
            split(mid+1,hi,arr,tmp); // get another part of the list
            merge(lo,mid,hi,arr,tmp); // merge list parts
        }
    }
    public static void merge(int lo,int mid,int hi,int[] arr,int[] tmp){ // lo - smallest array index
        // mid - middle array index
        // hi - highest array index
        // arr - array to be sorted
        // tmp - temporary array
        int i,j,k;
        i=lo;
        j=mid+1;
        k=0;
        for(;i<=mid && j<=hi;){
            if(arr[i]<arr[j]){ // smaller element left list
                tmp[k++]=arr[i++];
            }else if(arr[i]>arr[j]){ // smaller element right list
                tmp[k++]=arr[j++];
            }else{ // elements are same
                tmp[k++]=arr[i++];
            }
        }
        for(;i<=mid;){
            tmp[k++]=arr[i++];
        }
        for(;j<=hi;){
            tmp[k++]=arr[j++];
        }
        for(i=lo,k=0;i<=hi;){
            arr[i++]=tmp[k++];
        }
    }
}
